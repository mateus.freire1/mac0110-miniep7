# MAC0110 - MiniEP7
# Mateus Santos Freire - 11796889

function potencia(x, n)
  res = 1
  while n > 0
    res = res * x
    n = n - 1
  end
  return res
end

function fatorial(n)
    if n <= 1
        return 1
    else
        return n * fatorial(n - 1)
    end
end

function quaseigual(v1, v2)
    erro = 0.0001
    igual = abs(v1 - v2)
    if igual <= erro
        return true
    else
        return false
    end
end

function sin(x)
    soma = BigFloat(x)
    i = 1
    termo = BigFloat(x)
    for i in 1:10
        termo = -1 * termo * BigFloat(x) * BigFloat(x) / ((2 * i) * (2 * i + 1))
        soma = soma + termo
    end
    return BigFloat(soma)
end

function cos(x)
    soma = 1
    i = 1
    termo = 1
    for i in 1:10
        termo = -1 * termo * BigFloat(x) * BigFloat(x) / ((2 * i) * (2 * i - 1))
        soma = soma + termo
    end
    return BigFloat(soma)
end

function bernoulli(n)
   n *= 2
   A = Vector{Rational{BigInt}}(undef, n + 1)
   for m = 0 : n
      A[m + 1] = 1 // (m + 1)
      for j = m : -1 : 1
         A[j] = j * (A[j] - A[j + 1])
      end
   end
   return abs(A[1])
end

function tan(x)
    soma = 0
    i = 1
    termo = 1
    for i in 1:10
        termo = (-4^i) * (1 - 4^i) * bernoulli(i) * BigFloat(x)^(2 * i - 1) / fatorial(2 * i)
        soma = soma + BigFloat(termo)
    end
    return BigFloat(soma)
end

function check_sin(value, x)
   if quaseigual(value, sin(x))
      return true
   else
      return false
   end
end

function check_cos(value, x)
   if quaseigual(value, cos(x))
      return true
   else
      return false
   end
end

function check_tan(value, x)
   if quaseigual(value, tan(x))
      return true
   else
      return false
   end
end

function taylor_sin(x)
    soma = BigFloat(x)
    i = 1
    termo = BigFloat(x)
    for i in 1:10
        termo = -1 * termo * BigFloat(x) * BigFloat(x) / ((2 * i) * (2 * i + 1))
        soma = soma + termo
    end
    return BigFloat(soma)
end

function taylor_cos(x)
    soma = 1
    i = 1
    termo = 1
    for i in 1:10
        termo = -1 * termo * BigFloat(x) * BigFloat(x) / ((2 * i) * (2 * i - 1))
        soma = soma + termo
    end
    return BigFloat(soma)
end

function taylor_tan(x)
    soma = 0
    i = 1
    termo = 1
    for i in 1:10
        termo = (-4^i) * (1 - 4^i) * bernoulli(i) * BigFloat(x)^(2 * i - 1) / fatorial(2 * i)
        soma = soma + BigFloat(termo)
    end
    return BigFloat(soma)
end

function test()
    if !quaseigual(sin(pi / 2), 1)
       !quaseigual(cos(pi / 6), 0.866)
       !quaseigual(tan(pi / 4), 1)
       !quaseigual(sin(pi / 5), 0.587)
       !quaseigual(cos(pi / 4), 0.707)
       !quaseigual(tan(pi / 3), 1.732)
        return "Erro nas funções da parte 1"
    end
    if !check_sin(0.5, pi / 6) ||
            !check_cos(0.5, pi/3) ||
            !check_tan(1, pi/4) ||
            check_sin(1, pi) ||
            check_cos(0.5, pi) ||
            check_tan(20, pi / 4)
        return "Erro nas funções da parte 2"
    end
   return "Final dos testes"
end

println(test())

